<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="Web_Search.search" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divSearch" style ="background-color:grey" >    
        <table><tr><td>
             <asp:Label ID="lblSearch" Text="Enter Title" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
        <asp:TextBox ID="txtSearch" runat="server" Font-Names="verdana" BorderColor="Silver"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Search Text" ControlToValidate="txtSearch" Font-Names="verdana" Font-Bold="true" Font-Size="X-Small"></asp:RequiredFieldValidator>
        <asp:Button ID="btSearch" runat="server"  OnClick="Button1_Click" Text="Search" Font-Names="verdana" BackColor="Wheat" Font-Size="X-Small"/>
                   </td></tr>
       <tr><td>
        <asp:Label ID="Label1" Text="Titles" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
        <asp:DropDownList ID="ddTitles" runat="server" BackColor="Silver" Font-Size="X-Small" AutoPostBack="true" >
        </asp:DropDownList>
           <asp:Button ID="Button1" runat="server" Text="Details" Font-Names="verdana" BackColor="Wheat" Font-Size="X-Small" OnClick="Button1_Click1" />
           </td></tr>
            <tr><td>      
                <table border="1"><tr>
                     <td>
                 
                   <asp:Label ID="lbl1" Text="TitleId" Font-Bold="true" Width="100"  Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>    
                     <td id="Td9" runat="server">                         
                   <asp:Label ID="Label2" Text="TitleName" Font-Bold="true" Width="104" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>
                  <td id="Td10" runat="server">
                   <asp:Label ID="Label3" Text="SortTitleName" Font-Bold="true" Width="103" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>
                  <td id="Td11" runat="server">
                   <asp:Label ID="Label4" Text="TitleTypeid" Font-Bold="true" Width="100" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>
                  <td id="Td12" runat="server">
                   <asp:Label ID="Label5" Text="ReleaseYear" Font-Bold="true" Width="105" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>
                  <td id="Td14" runat="server">
                   <asp:Label ID="Label6" Text="ProcessedDatetime" Font-Bold="true" Width="150" Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" runat="server"></asp:Label>
             </td>           
                       </tr></table>         
            <asp:ListView runat="server" ID="lstTitle">

            <LayoutTemplate>
                     <table id="Table1"  runat="server" border="1" cellpadding="2" style="width:460px">       
              <tr  id="itemPlaceholder"  runat="server">                 
              </tr>
            </table>
          </LayoutTemplate>
          <ItemTemplate>              
            <tr  runat="server">
              <td id="Td7" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="TitleId" runat="server" Width="100"  Text='<%#Eval("TitleId") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" />
              </td>
                 <td id="Td2" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="titleName" runat="server" Width="100" Text='<%#Eval("titleName") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon"  />
              </td>
                 <td id="Td3" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="titlenamesortable" runat="server" Width="100" Text='<%#Eval("titlenamesortable") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" />
              </td>                
                 <td id="Td4" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="titletypeid" runat="server"  Width="100" Text='<%#Eval("titletypeid") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" />
              </td>
                
                 <td id="Td5" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="releaseyear" runat="server" Width="100" Text='<%#Eval("releaseyear") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" />
              </td>
                
                 <td id="Td6" runat="server">
                <%-- Data-bound content. --%>
                <asp:Label ID="processeddatetimeutc" runat="server" Width="150" Text='<%#Eval("processeddatetimeutc") %>' Font-Names="verdana" Font-Size="X-Small" ForeColor="Maroon" />
              </td>
                
    </tr>
  </ItemTemplate>
                </asp:ListView>
                </td></tr>
            
            </table>
        </div>
        
    </form>
</body>
</html>
