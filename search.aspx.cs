using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Web;



namespace Web_Search
{
    public partial class search : System.Web.UI.Page
    {
        SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder();
        SqlDataAdapter daAdapter;
        DataSet dsDataSet;
        SqlCommand cmdSQL;
        protected void Page_Load(object sender, EventArgs e)
        {          
            connStringBuilder.DataSource = "tcp:bx8cna5bk0.database.windows.net,1433";
            connStringBuilder.InitialCatalog = "Titles";
            connStringBuilder.MultipleActiveResultSets = true;           
            connStringBuilder.UserID = "readuser@bx8cna5bk0";
            connStringBuilder.Password = "read!234#Q~$";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            

            //Building connection string
           
            //Open the connection and load data into dataset.
            using (SqlConnection conn = new SqlConnection(connStringBuilder.ToString()))
            {
                using (SqlCommand command = conn.CreateCommand())
                {
                    conn.Open();

                    cmdSQL = new SqlCommand("SELECT * FROM Title where TitleName like '" + txtSearch.Text.Trim() + "%'", conn);
                    daAdapter = new SqlDataAdapter(cmdSQL);
                    dsDataSet = new DataSet();
                    daAdapter.Fill(dsDataSet);
                    if (dsDataSet.Tables[0].Rows.Count !=0)
                    {
                        ddTitles.Items.Clear();
                        ddTitles.DataSource = dsDataSet.Tables[0];
                        ddTitles.DataTextField = dsDataSet.Tables[0].Columns["TitleName"].ToString();
                        ddTitles.DataValueField = dsDataSet.Tables[0].Columns["TitleId"].ToString();                        
                        ddTitles.DataBind();
                        ddTitles.SelectedItem.Enabled = true;
                    }
                    else
                    {
                        ddTitles.Items.Clear();
                        ddTitles.Items.Add("No Titles available");    
                    }                   
                }
                conn.Close();
            }
        }
               

        protected void Button1_Click1(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connStringBuilder.ToString()))
            {
                using (SqlCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    cmdSQL = new SqlCommand("SELECT TitleId,titleName,titlenamesortable,titletypeid,releaseyear,processeddatetimeutc FROM Title where TitleId =" + ddTitles.SelectedValue.ToString().Trim() , conn);
                    daAdapter = new SqlDataAdapter(cmdSQL);
                    dsDataSet = new DataSet();
                    daAdapter.Fill(dsDataSet);
                    if (dsDataSet.Tables[0].Rows.Count != 0)
                    {
                        lstTitle.DataSource = dsDataSet.Tables[0];
                        lstTitle.DataBind();
                    }
                }
                conn.Close();
            }
        }
              
       
    }
}